package com.newsapp.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.newsapp.model.Favourite;
import com.newsapp.service.FavService;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class FavControllerTest {

	private MockMvc mockMvc;
    @Mock
    FavService service;
    @InjectMocks
    private FavController favcontroller;
    
    private Favourite favourite;
    private List<Favourite> newslist;
    
    @BeforeEach
    public void setUp() {
    	MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(favcontroller).build();
        favourite = new Favourite();
    	favourite.setFavid(45);
    	favourite.setTitle("DemoNews");
    	favourite.setUrl("demoUrl");
    	favourite.setUrlToImage("demoImgUrl");
    	favourite.setDescription("demoDescription");
    	favourite.setUserid(3);
    	newslist = new ArrayList<>();
    	newslist.add(favourite);
    }
    
    @AfterEach
    public void tearDown() {
        favourite = null;
    }
    
    @Test
    public void givenNewsToSaveThenShouldReturnSavedNews() throws Exception {
        when(service.saveFav(any())).thenReturn(favourite);
        mockMvc.perform(post("/addtolist")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(favourite)))
                .andExpect(status().isCreated())
                .andDo(MockMvcResultHandlers.print());
        verify(service).saveFav(any());
    }
    
    
    @Test
    public void givenUserIdThenShouldReturnAllFavouriteNewsOfThatUser() throws Exception {
        when(service.getAllFavNews(3)).thenReturn(newslist);
        mockMvc.perform(MockMvcRequestBuilders.get("/favlist/3")
                .contentType(MediaType.APPLICATION_JSON).content(asJsonString(favourite)))
                .andDo(MockMvcResultHandlers.print());
        verify(service).getAllFavNews(3);
        verify(service, times(1)).getAllFavNews(3);

    }
    
    @Test
    public void givenFavIdThenShouldDeleteTheItem() throws Exception {
        when(service.deleteFavNewsById(favourite.getFavid())).thenReturn(favourite);
        mockMvc.perform(delete("/favlist/45")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(favourite)))
                .andExpect(MockMvcResultMatchers.status().isOk()).andDo(MockMvcResultHandlers.print());
    }
    
    @Test
    public void ShouldReturnAllRecommendedNews() throws Exception {
        when(service.getAllRecNews()).thenReturn(newslist);
        mockMvc.perform(MockMvcRequestBuilders.get("/recommended")
                .contentType(MediaType.APPLICATION_JSON).content(asJsonString(favourite)))
                .andDo(MockMvcResultHandlers.print());
        verify(service).getAllRecNews();
        verify(service, times(1)).getAllRecNews();

    }
    
    
    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
