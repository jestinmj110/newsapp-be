package HomeTest;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


import PageObjectModel.HomePageObjectModel;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

public class HomeStepDefinition {

	WebDriver chromeDriver = null;
	HomePageObjectModel homepage = null;

	@Before // Hooks
	public void setUp() {
		WebDriverManager.chromedriver().setup();
		chromeDriver = new ChromeDriver();

	}
	
	@Given("Login page for App")
	public void login_page_for_app() {
		chromeDriver.get("http://localhost:4200/login");
		homepage = new HomePageObjectModel(chromeDriver);
		assertEquals(chromeDriver.getCurrentUrl(), "http://localhost:4200/login");
	}

	@When("user enters the email and password")
	public void user_enters_the_email_and_password(io.cucumber.datatable.DataTable dataTable) {
		List<String> aslist = dataTable.asList();
		homepage.enterEmail(aslist.get(0));
		homepage.enterPassword(aslist.get(1));
	}

	@When("user clicks login button")
	public void user_clicks_login_button() throws InterruptedException {
		homepage.clicksonSubmit();
		Thread.sleep(500);
		chromeDriver.switchTo().alert().accept();
		Thread.sleep(5000);
	}

	@When("user clicks on Category button")
	public void user_clicks_on_category_button() throws InterruptedException {
		homepage.clicksonCategory();
		Thread.sleep(5000);
	}

	@Then("should navigate to News page")
	public void should_navigate_to_news_page() {
		assertEquals(chromeDriver.getCurrentUrl(), "http://localhost:4200/news");
	}
	

	@When("user clicks on favourite button")
	public void user_clicks_on_favourite_button() {
		homepage.clicksonAddto();
	}

	@Then("should return alert message")
	public void should_return_alert_message() throws InterruptedException {
		Thread.sleep(500);
		assertEquals("Added to Your List",chromeDriver.switchTo().alert().getText());
		chromeDriver.switchTo().alert().accept();
	}


	@Then("should return warning alert message")
	public void should_return_warning_alert_message() throws InterruptedException {
		Thread.sleep(500);
		assertEquals("News is Already in your List",chromeDriver.switchTo().alert().getText());
		chromeDriver.switchTo().alert().accept();
	}


	@When("user clicks on Favourite navbar button")
	public void user_clicks_on_favourite_navbar_button() throws InterruptedException {
		
		homepage.clicksonFavourite();
	}
	

	@Then("should navigate to favourite page")
	public void should_navigate_to_favourite_page() {
		assertEquals(chromeDriver.getCurrentUrl(), "http://localhost:4200/favourite");
	}


	@When("user clicks on Recommended button")
	public void user_clicks_on_recommended_button() {
		homepage.clicksonRecommend();
	}

	@Then("should navigate to Recommended page")
	public void should_navigate_to_recommended_page() {
		assertEquals(chromeDriver.getCurrentUrl(), "http://localhost:4200/recommended");
	}
	
	@After
	public void closeBrowser() {
		chromeDriver.close();
	}

}
