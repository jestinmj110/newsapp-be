package com.newsapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class FavouriteListApplication {

	public static void main(String[] args) {
		SpringApplication.run(FavouriteListApplication.class, args);
	}

}
