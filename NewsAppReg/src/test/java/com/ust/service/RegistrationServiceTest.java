package com.ust.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import com.ust.model.User;
import com.ust.repository.RegistrationRepository;


@ExtendWith(MockitoExtension.class)
public class RegistrationServiceTest {
	
	@Mock
	private RegistrationRepository repo;
	
	@Autowired
	private User user1;
	
	@InjectMocks
	 RegistrationService service;
	
	private List<User> list;
	
	@BeforeEach
	void setUp() throws Exception {
		BigInteger mobile=new BigInteger("9714414922");
		user1=new User(39,"test66@gmail.com",mobile,"tester","root");		
		list=new ArrayList<>();
		list.add(user1);
	
	}
	
	@AfterEach
	void tearDown() throws Exception {
		user1=null;
		
	}

	@Test
	void givenUserThenReturnRegisteredUser() {
		
		repo.save(user1);
		when(repo.findAll()).thenReturn(list);
		List<User> cusList = repo.findAll();
		assertEquals("tester",cusList.get(0).getUsername());
		
		
		verify(repo,times(1)).save(user1);
		verify(repo,times(1)).findAll();
	}
	

	
	
	@Test
	void findUserByEmailId() {
		when(repo.findByEmailId("test66@gmail.com")).thenReturn(user1);
		User findUser = service.fetchUserByEmailId("test66@gmail.com");
		
		assertEquals("test66@gmail.com", findUser.getEmailId());
		assertEquals("tester", findUser.getUsername());
		
	}
	
	@Test
	void getUserByEmailAndPassword(){
		when(repo.findByEmailIdAndPassword("test66@gmail.com", "root")).thenReturn(user1);
		User getUser=service.fetchUserByEmailIdAndPssword("test66@gmail.com","root");
		
		assertEquals("test66@gmail.com", getUser.getEmailId());
		assertEquals("tester", getUser.getUsername());


	}

}
