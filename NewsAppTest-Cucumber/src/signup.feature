Feature: Test the SignUp functionality of BookApp
Scenario: SignUp with valid credentials
	Given SignUp page for BookApp
	When user enters the name , phone , email and password
			| tester	| 9876543210  | tester@gmail.com	| root	|
	And user clicks SignUp button
	Then should navigate to home page
	
Scenario: SignUp with Invalid credentials
	Given SignUp page for BookApp
	When user enters the name , phone , email and password
			| tester	| 9876543210  | abc@gmail.com	| root	|
	And user clicks SignUp button
	Then should return error message

Scenario: SignUp with no credentials
	Given SignUp page for BookApp
	When user clicks SignUp button
	Then should return alert message

Scenario: SignUp with Email only
	Given SignUp page for BookApp
	When user enters the email
		| abc@gmail.com	|
	And user clicks SignUp button
	Then should return alert message
	
Scenario: SignUp with password only
	Given SignUp page for BookApp
	When user enters the password
		| abcd	|
	And user clicks SignUp button
	Then should return alert message
	