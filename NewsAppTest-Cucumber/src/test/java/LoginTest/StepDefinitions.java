package LoginTest;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import PageObjectModel.LoginPageObjectModel;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

public class StepDefinitions {
	WebDriver chromeDriver = null;
	LoginPageObjectModel loginpage = null;

	@Before // Hooks
	public void setUp() {
		WebDriverManager.chromedriver().setup();
		chromeDriver = new ChromeDriver();

	}

	@Given("Login page for NewsApp")
	public void login_page_for_news_app() {
		chromeDriver.get("http://localhost:4200/login");
		loginpage = new LoginPageObjectModel(chromeDriver);
		assertEquals(chromeDriver.getCurrentUrl(), "http://localhost:4200/login");
	}

	@When("user enters the email and password")
	public void user_enters_the_email_and_password(io.cucumber.datatable.DataTable dataTable) {
		List<String> aslist = dataTable.asList();
		loginpage.enterEmail(aslist.get(0));
		loginpage.enterPassword(aslist.get(1));
	}
	
	@When("user enters the email")
	public void user_enters_the_email(io.cucumber.datatable.DataTable dataTable) {
		List<String> aslist = dataTable.asList();
		loginpage.enterEmail(aslist.get(0));
	}
	
	@When("user enters the password")
	public void user_enters_the_password(io.cucumber.datatable.DataTable dataTable) {
		List<String> aslist = dataTable.asList();
		loginpage.enterPassword(aslist.get(0));
	}

	@When("user clicks login button")
	public void user_clicks_login_button() throws NoAlertPresentException,InterruptedException{

		loginpage.clicksonSubmit();
		Thread.sleep(2000);
	}

	@Then("should navigate to home page")
	public void should_navigate_to_home_page() throws NoAlertPresentException,InterruptedException{
		
		Alert alert = chromeDriver.switchTo().alert();
		String alertMessage= chromeDriver.switchTo().alert().getText();
		System.out.println(alertMessage); // Print Alert Message
		Thread.sleep(2000);
		alert.accept();
		assertEquals("Login success", alertMessage);
	}
	
	@Then("should return error message")
	public void should_return_error_message() throws InterruptedException {
		Thread.sleep(2000);
		assertEquals("Invalid Credentials, Please enter Email and Password",chromeDriver.switchTo().alert().getText());
		chromeDriver.switchTo().alert().accept();
	}
	
	@Then("should return alert message")
	public void should_return_alert_message() throws NoAlertPresentException,InterruptedException {
		Thread.sleep(1000);
		assertEquals("Please enter Your details!",chromeDriver.switchTo().alert().getText());
		chromeDriver.switchTo().alert().accept();
	}


	@After
	public void closeBrowser() {
		chromeDriver.close();
	}

}

