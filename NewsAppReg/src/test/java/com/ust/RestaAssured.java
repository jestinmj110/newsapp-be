package com.ust;

import org.testng.AssertJUnit;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.minidev.json.JSONObject;

public class RestaAssured {
	

@Test
 void LoginCheck() {
	RestAssured.baseURI="http://localhost:9000";
	RequestSpecification httpRequest=RestAssured.given();
	
	JSONObject requestParams=new  JSONObject();
	requestParams.put("emailId","admin@gmail.com");
	requestParams.put("password","admin");
	
	httpRequest.header("Content-Type","application/json");
	httpRequest.body(requestParams.toJSONString());
	
	Response response=httpRequest.request(Method.POST,"/loginuser");
	String responseBody=response.getBody().asString();
	System.out.println("Response Body is" + responseBody);
	
	int statusCode=response.getStatusCode();
	System.out.println("status code is: "+statusCode);
	AssertJUnit.assertEquals(statusCode, 200);
	
	String statusLine=  response.getStatusLine();
	System.out.println(statusLine);
	AssertJUnit.assertEquals(statusLine, "HTTP/1.1 200 ");
	

} 

   @Test
   void RegistrationCheck() {
   RestAssured.baseURI="http://localhost:9000";
	RequestSpecification httpRequest=RestAssured.given();
	
	JSONObject requestParams=new  JSONObject();
	requestParams.put("id", 1);
	requestParams.put("emailId","test19@gmail.com");
	requestParams.put("mobile","12345687");
	requestParams.put("username","tester");
	requestParams.put("password","tester");
	
	httpRequest.header("Content-Type","application/json");
	httpRequest.body(requestParams.toJSONString());
	
	Response response=httpRequest.request(Method.POST,"/registeruser");
	String responseBody=response.getBody().asString();
	System.out.println("Response Body is" + responseBody);
	
	int statusCode=response.getStatusCode();
	System.out.println("status code is: "+statusCode);
	AssertJUnit.assertEquals(statusCode, 500);
	
	String statusLine=  response.getStatusLine();
	System.out.println(statusLine);
	AssertJUnit.assertEquals(statusLine, "HTTP/1.1 500 ");
}
}
