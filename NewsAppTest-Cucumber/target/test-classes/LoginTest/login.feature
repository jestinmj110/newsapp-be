Feature: Test the Login functionality of NewsApp
Scenario: Login with valid credentials
	Given Login page for NewsApp
	When user enters the email and password
		| admin@gmail.com	| admin |
	And user clicks login button
	Then should navigate to home page
	
Scenario: Login with Invalid credentials
	Given Login page for NewsApp
	When user enters the email and password
		| abc@gmail.com	| xxx |
	And user clicks login button
	Then should return error message

Scenario: Login with no credentials
	Given Login page for NewsApp
	When user clicks login button
	Then should return alert message

Scenario: Login with Email only
	Given Login page for NewsApp
	When user enters the email
		| abc@gmail.com	|
	And user clicks login button
	Then should return alert message
	
Scenario: Login with password only
	Given Login page for NewsApp
	When user enters the password
		| abcd	|
	And user clicks login button
	Then should return alert message


	