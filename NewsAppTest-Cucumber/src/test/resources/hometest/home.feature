Feature: Test the Home functionality of App

Scenario: Home page with valid credentials
	Given Login page for App
	When user enters the email and password
		| admin@gmail.com	| admin |
	And user clicks login button
	And user clicks on Category button
	Then should navigate to News page 
	
Scenario: Add to favourite with valid credentials
	Given Login page for App
	When user enters the email and password
		| admin@gmail.com	| admin |
	And user clicks login button
	And user clicks on Category button
	And user clicks on favourite button
	Then should return alert message

Scenario: Add to favourite an already existing item
	Given Login page for App
	When user enters the email and password
		| admin@gmail.com	| admin |
	And user clicks login button
	And user clicks on Category button
	And user clicks on favourite button
	Then should return warning alert message
	
 Scenario: Home from favourite page
	Given Login page for App
	When user enters the email and password
		| admin@gmail.com	| admin |
	And user clicks login button
	And user clicks on Favourite navbar button
	Then should navigate to favourite page
	
Scenario: Recommended from favourite page
	Given Login page for App
	When user enters the email and password
		| admin@gmail.com	| admin |
	And user clicks login button
	And user clicks on Recommended button
	Then should navigate to Recommended page