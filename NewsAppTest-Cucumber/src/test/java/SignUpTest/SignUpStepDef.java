package SignUpTest;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


import PageObjectModel.SignupPageObjectModel;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

public class SignUpStepDef {
	
	WebDriver chromeDriver = null;
	SignupPageObjectModel signuppage = null;

	@Before // Hooks
	public void setUp() {
		WebDriverManager.chromedriver().setup();
		chromeDriver = new ChromeDriver();

	}

	@Given("SignUp page for NewsApp")
	public void sign_up_page_for_news_app() {
		chromeDriver.get("http://localhost:4200/signup");
		signuppage = new SignupPageObjectModel(chromeDriver);
		assertEquals(chromeDriver.getCurrentUrl(), "http://localhost:4200/signup");
	    
	}
	
	@When("user enters the name , phone , email and password")
	public void user_enters_the_name_phone_email_and_password(io.cucumber.datatable.DataTable dataTable) {
		List<String> aslist = dataTable.asList();
		signuppage.enterName(aslist.get(0));
		signuppage.enterPhone(aslist.get(1));
		signuppage.enterEmail(aslist.get(2));
		signuppage.enterPassword(aslist.get(3));
		signuppage.enterConfirm(aslist.get(4));
	}
	
	@When("user enters the email")
	public void user_enters_the_email(io.cucumber.datatable.DataTable dataTable) {
		List<String> aslist = dataTable.asList();
		signuppage.enterEmail(aslist.get(0));
	}
	@When("user enters the password")
	public void user_enters_the_password(io.cucumber.datatable.DataTable dataTable) { 
		List<String> aslist = dataTable.asList();
		signuppage.enterPassword(aslist.get(0));
	}
	@When("user clicks SignUp button")
	public void user_clicks_sign_up_button() throws NoAlertPresentException,InterruptedException {
		
		Thread.sleep(1000);
		signuppage.clicksonSubmit();
	}

	@When("user upload image")
	public void user_upload_image() throws InterruptedException {
		Thread.sleep(1000);
	    signuppage.uploadImg();
	}



	@Then("should return error message")
	public void should_return_error_message() {
		WebElement message = signuppage.getMessage();
		System.out.println(message.getText());
		assertEquals("Please Upload Profile picture", message.getText());
	}
	
	@Then("should navigate to home page")
	public void should_navigate_to_home_page() throws NoAlertPresentException,InterruptedException {
		Thread.sleep(1000);
		Alert alert = chromeDriver.switchTo().alert();
		String alertMessage= chromeDriver.switchTo().alert().getText();
		System.out.println(alertMessage); // Print Alert Message
		Thread.sleep(2000);
		alert.accept();
		assertEquals("Registration successful", alertMessage);
		
	    
	}
	
	@Then("should return alert message")
	public void should_return_alert_message() throws NoAlertPresentException,InterruptedException {
		Thread.sleep(2000);
		assertEquals("Please enter Your details",chromeDriver.switchTo().alert().getText());
		chromeDriver.switchTo().alert().accept();
	}
	
	@After
	public void closeBrowser() {
		chromeDriver.close();
	}
}






