package com.newsapp.controller;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.newsapp.model.Favourite;
import com.newsapp.service.FavService;

@SpringBootTest
public class FavControllerIntegrationTest {
	
    @Autowired
    private FavService service;
    private Favourite favourite;
    private List<Favourite> newslist;
    
    @BeforeEach
    public void setUp() {
    	favourite = new Favourite();
    	favourite.setFavid(45);
    	favourite.setTitle("DemoNews");
    	favourite.setUrl("demoUrl");
    	favourite.setUrlToImage("demoImgUrl");
    	favourite.setDescription("demoDescription");
    	favourite.setUserid(3);
    	newslist = new ArrayList<>();
    	newslist.add(favourite);
    }
    
    @AfterEach
    public void tearDown() {
        favourite = null;
    }
    
    @Test
    void givenNewsToSaveThenShouldReturnTheSavedNews() throws Exception {
    	Favourite savedFavourite = service.saveFav(favourite);
    	assertNotNull(savedFavourite);
    	assertEquals(favourite.getFavid(),savedFavourite.getFavid());
    	service.deleteFavNewsById(savedFavourite.getFavid());
    }

    @Test
    public void givenUserIdThenShouldReturnAllFavouriteNewsOfThatUser() throws Exception {
    	List<Favourite> newsList = service.getAllFavNews(1);
    	assertNotNull(newsList);
    }
    
    @Test
    public void givenFavIdThenShouldDeleteTheItem() throws Exception {
    	Favourite deletedFavourite = service.deleteFavNewsById(11);
    	assertNotNull(deletedFavourite);
    	    	
    }
    
    @Test
    public void ShouldReturnAllRecommendedNews() throws Exception {
    	List<Favourite> newsList = service.getAllRecNews();
    	assertNotNull(newsList);
    }
    
}
