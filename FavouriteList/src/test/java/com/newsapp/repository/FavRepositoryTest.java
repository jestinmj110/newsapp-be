package com.newsapp.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;


import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.newsapp.model.Favourite;


@SpringBootTest
public class FavRepositoryTest {
	
	@Autowired
    private FavRepository repo;
    private Favourite favourite;
    
    @BeforeEach
    public void setUp() {
    	favourite = new Favourite();
    	favourite.setFavid(45);
    	favourite.setTitle("DemoNews");
    	favourite.setUrl("demoUrl");
    	favourite.setUrlToImage("demoImgUrl");
    	favourite.setDescription("demoDescription");
    	favourite.setUserid(3);
    	
    }
    
    @AfterEach
    public void tearDown() {
        favourite = null;
    }
    
    @Test
    public void givenBlogToSaveThenShouldReturnSavedBlog() {
        repo.save(favourite);
        Favourite fetchednews = repo.findById(favourite.getFavid()).get();
        assertEquals(45, fetchednews.getFavid());
    }
    
    @Test
    public void givenGetAllBlogsThenShouldReturnListOfAllBlogs() {
    	Favourite favourite1 = new Favourite(46,3,"demotitle1","demodescription1", "demoUrl1" , "demoImgUrl1" );
    	Favourite favourite2 = new Favourite(47,4,"demotitle2","demodescription2", "demoUrl2" , "demoImgUrl2" );
        repo.save(favourite1);
        repo.save(favourite2);

        List<Favourite> newsList = (List<Favourite>) repo.findAll();
        assertEquals(8, newsList.get(1).getUserid());
    }

}
