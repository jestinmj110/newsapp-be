package com.newsapp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.newsapp.model.Favourite;
import com.newsapp.repository.FavRepository;


@Service
public class FavService {
	
	@Autowired
	public FavRepository repo;
	
	
	public Favourite saveFav(Favourite fav) {
		return repo.save(fav);
	}
	
	public Favourite fetchListByTitleAndUserid(String title,int userid) {
		return repo.findByTitleAndUserid(title,userid);
	}
	
	public List<Favourite> getAllFavNews(int user) {
		return repo.findByUserid(user);
	}
	public Favourite deleteFavNewsById(int id) {
		Optional<Favourite> newsById = repo.findById(id);
		repo.deleteById(id);
		if (newsById.isPresent()) {
			return newsById.get();
		}else {
			return null;
		}
	}
	public Favourite fetchUserById(int fav) {
		return repo.findByFavid(fav);
	}
	public List<Favourite> getAllRecNews() {
		
		return repo.findAll();
	}
}
