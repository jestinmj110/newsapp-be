Feature: Test the SignUp functionality of NewsApp
Scenario: SignUp with valid credentials
	Given SignUp page for NewsApp
	When user enters the name , phone , email and password
			| tester	| 9876543210  | test111@gmail.com	| root	| root	|
	And user upload image
	And user clicks SignUp button
	Then should navigate to home page
	
Scenario: SignUp with Invalid credentials
	Given SignUp page for NewsApp
	When user enters the name , phone , email and password
			| tester	| 9876543210  | admin@gmail.com	| root	| root	|
	And user clicks SignUp button
	Then should return error message

Scenario: SignUp with no credentials
	Given SignUp page for NewsApp
	When user clicks SignUp button
	Then should return alert message

Scenario: SignUp with Email only
	Given SignUp page for NewsApp
	When user enters the email
		| abc@gmail.com	|
	And user clicks SignUp button
	Then should return alert message
	
Scenario: SignUp with password only
	Given SignUp page for NewsApp
	When user enters the password
		| abcd	|
	And user clicks SignUp button
	Then should return alert message
	