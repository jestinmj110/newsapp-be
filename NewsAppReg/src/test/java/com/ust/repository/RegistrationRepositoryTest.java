package com.ust.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigInteger;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.ust.model.User;
import com.ust.service.RegistrationService;


@SpringBootTest
public class RegistrationRepositoryTest {
	
	@Autowired
	private RegistrationRepository repo;
	private User user;
	
	@BeforeEach // method should be executed before each @test
	void setUp() {
		user=new User();
		
		user.setId(20);
		user.setUsername("cr7");
		user.setEmailId("cr@gmail.com");
		user.setPassword("root");
		BigInteger mobile=new BigInteger("971441492");
		user.setMobile(mobile);
		
		
	}

	@AfterEach // method should be executed after each @Test
	void tearDown() {
		user=null;
	}
	
	 @Test
	 public void givenBlogToSaveThenShouldReturnSavedBlog() {
	        repo.save(user);
	        
	 }

}

