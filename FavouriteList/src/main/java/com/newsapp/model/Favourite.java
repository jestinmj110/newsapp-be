package com.newsapp.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Favourite {
	
	@Id
	private int favid;
	private int userid;
	private String title;
	private String description;
	private String url;
	private String urlToImage;
	
	public Favourite() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Favourite(int favid, int userid, String title, String description, String url, String urlToImage) {
		super();
		this.favid = favid;
		this.userid = userid;
		this.title = title;
		this.description = description;
		this.url = url;
		this.urlToImage = urlToImage;
	}
	public Favourite( int userid, String title, String description, String url, String urlToImage) {
		super();
		this.userid = userid;
		this.title = title;
		this.description = description;
		this.url = url;
		this.urlToImage = urlToImage;
	}

	public int getFavid() {
		return favid;
	}

	public void setFavid(int favid) {
		this.favid = favid;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUrlToImage() {
		return urlToImage;
	}

	public void setUrlToImage(String urlToImage) {
		this.urlToImage = urlToImage;
	}
	
	
	

}
