package PageObjectModel;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class FavouritePageObjectModel {
	public WebDriver driver;

	public FavouritePageObjectModel(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getEmail() {
		return driver.findElement(By.xpath("//input[contains(@name,'email')]"));
	}

	public WebElement getPassword() {
		return driver.findElement(By.xpath("//input[contains(@type,'password')]"));
	}
	public WebElement getSubmitButton() {
		return driver.findElement(By.xpath("//button[contains(@type,'submit')]"));
	}
	
	public WebElement getFavouriteButton() {
		return driver.findElement(By.xpath("//a[contains(@name,'favourite')]"));
	}
	
	public WebElement getRemoveButton() {
		return driver.findElement(By.xpath("/html/body/app-root/app-favourite/div/div/div[1]/div[2]/button"));
	}
	
	public WebElement getHomeButton() {
		return driver.findElement(By.xpath("//a[contains(@name,'home')]"));
	}
	
	public WebElement getRecommendButton() {
		return driver.findElement(By.xpath("//a[contains(@name,'recommended')]"));
	}
	
	public void clicksonRecommend() {
		getRecommendButton().click();
	}
	
	public void clicksonHome() {
		getHomeButton().click();
	}
	
	public void clicksonRemove() {
		getRemoveButton().click();
	}
	
	public void clicksonSubmit() {
		getSubmitButton().click();
	}
	public void clicksonFavourite() {
		getFavouriteButton().click();
	}
	
	public void enterEmail(String mail) {
		getEmail().sendKeys(mail);
	}
	
	public void enterPassword(String pass) {
		getPassword().sendKeys(pass);
	}
	


}
