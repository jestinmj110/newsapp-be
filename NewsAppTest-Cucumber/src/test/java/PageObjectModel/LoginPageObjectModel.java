package PageObjectModel;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPageObjectModel {
	public WebDriver driver;

	public LoginPageObjectModel(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getEmail() {
		return driver.findElement(By.xpath("//input[contains(@name,'email')]"));
	}

	public WebElement getPassword() {
		return driver.findElement(By.xpath("//input[contains(@type,'password')]"));
	}
	public WebElement getMessage() {
		return driver.findElement(By.xpath("//small[contains(@name,'message')]"));
	}

	public void enterEmail(String mail) {
		getEmail().sendKeys(mail);
	}
	
	public void enterPassword(String pass) {
		getPassword().sendKeys(pass);
	}
	
	public WebElement getSubmitButton() {
		return driver.findElement(By.xpath("//button[contains(@type,'submit')]"));
	}
	
	public WebElement getNewUserButton() {
		return driver.findElement(By.xpath("//a[contains(@herf,\"\")]"));
	}

	public void clicksonSubmit() {
		getSubmitButton().click();
	}
	public void clicksonNewUsert() {
		getNewUserButton().click();
	}
}
