package com.newsapp.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.newsapp.model.Favourite;
import com.newsapp.repository.FavRepository;

@SpringBootTest
public class FavServiceIntegrationTest {
	
	@Autowired
    private FavRepository repo;
	
    @Autowired
    private FavService service;
    private Favourite favourite1, favourite2, favourite3;
    private List<Favourite> favouriteList;
    
    @BeforeEach
    public void setUp() {
    	favouriteList = new ArrayList<>();
    	favourite1 = new Favourite(3,"demotitle1","demodescription1", "demoUrl1" , "demoImgUrl1" );
    	favourite2 = new Favourite(4,"demotitle2","demodescription2", "demoUrl2" , "demoImgUrl2" );
    	favourite3 = new Favourite(5,"demotitle3","demodescription3", "demoUrl3" , "demoImgUrl3" );
    	favouriteList.add(favourite1);
    	favouriteList.add(favourite2);
    	favouriteList.add(favourite3);
    }
    
    @AfterEach
    public void tearDown() {
    	favourite1 = favourite2 = favourite3 = null;
    	favouriteList = null;
    }
    
    @Test
    public void givenNewsToSaveThenShouldReturnSavedNews() {
    	Favourite savedFavourite = repo.save(favourite1);
    	assertNotNull(savedFavourite);
    	assertEquals(favourite1.getFavid(),savedFavourite.getFavid());
    	
    	
    }
    
    @Test
    public void givenNewsToSaveThenShouldReturnSavedNewsTryTwo() {
    	Favourite savedFavourite = repo.save(favourite2);
    	assertNotNull(savedFavourite);
    	assertEquals(favourite2.getFavid(),savedFavourite.getFavid());
    	
    	
    }
    
    @Test
    public void givenUserIdThenShouldReturnAllFavouriteNewsOfThatUser() throws Exception {
    	List<Favourite> newsList = repo.findByUserid(5);
    	assertNotNull(newsList);
    }
    
    @Test
    public void givenFavIdThenShouldDeleteTheItem() throws Exception {
    	Favourite deletedFavourite = repo.findByFavid(10);
    	repo.deleteByFavid(11);
    	assertNotNull(deletedFavourite);
    	    	
    }
    
    
}
