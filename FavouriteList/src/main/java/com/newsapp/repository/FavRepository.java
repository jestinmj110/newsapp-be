package com.newsapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.newsapp.model.Favourite;


public interface FavRepository extends JpaRepository<Favourite , Integer>{
	
	public Favourite findByTitleAndUserid(String title,int userid);

	public List<Favourite> findByUserid(int user);

	public void deleteByFavid(int favid);

	public Favourite findByFavid(int favid);
}
