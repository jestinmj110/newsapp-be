package com.newsapp.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import com.newsapp.model.Favourite;
import com.newsapp.repository.FavRepository;



@ExtendWith(MockitoExtension.class)
public class FavServiceTest {

	
	 @Mock
    private FavRepository repo;
	
	 @InjectMocks
    private FavService service;
    private Favourite favourite1, favourite2;
    private List<Favourite> newsList;
    private Optional optional;
    
    @BeforeEach
    public void setUp() {
    	MockitoAnnotations.openMocks(this);
    	favourite1 = new Favourite(3,"demotitle1","demodescription1", "demoUrl1" , "demoImgUrl1" );
    	favourite2 = new Favourite(43,4,"demotitle2","demodescription2", "demoUrl2" , "demoImgUrl2" );
    	 optional = Optional.of(favourite1);
    }
    
    @AfterEach
    public void tearDown() {
        favourite1 = favourite2 = null;
    }
    
    @Test
    public void givenNewsToSaveThenShouldReturnSavedNews() throws Exception {
        when(repo.save(any())).thenReturn(favourite1);
        assertEquals(favourite1, service.saveFav(favourite1));
        verify(repo, times(1)).save(any());
    }
    
    @Test
    public void givenNewsToSaveThenShouldNotReturnSavedNews() {
        when(repo.save(any())).thenThrow(new RuntimeException());
        Assertions.assertThrows(RuntimeException.class,() -> {
            service.saveFav(favourite1);
        });
        verify(repo, times(1)).save(any());
    }
    
    @Test
    public void givenGetAllNewsThenShouldReturnListOfAllNews() {
       	repo.save(favourite1);
        //stubbing the mock to return specific data
        when(repo.findAll()).thenReturn(newsList);
        List<Favourite> newsList1 = service.getAllRecNews();
    
        verify(repo, times(1)).save(favourite1);
        verify(repo, times(1)).findAll();
    }
    
    @Test
    void givenBlogIdToDeleteThenShouldReturnDeletedBlog() {
        when(repo.findById(favourite2.getFavid())).thenReturn(optional);
        Favourite deletednews = service.deleteFavNewsById(43);
       
        
        verify(repo, times(1)).findById(favourite2.getFavid());
        verify(repo, times(1)).deleteById(favourite2.getFavid());
    }
   
    
    
}
