package com.ust.controller;

import java.math.BigInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import com.ust.model.User;
import com.ust.service.RegistrationService;

@RestController
public class RegistrationController {
	
	@Autowired
	private RegistrationService service;
	
	@GetMapping("/home")
	public String msg() {
		return "home";
	}
	
	@PostMapping(path = "/registeruser" )
	@CrossOrigin(origins ="http://localhost:4200")
	public ResponseEntity<User> registerUser(@RequestBody User user) throws Exception {
		String tempEmailId = user.getEmailId();
		if (tempEmailId != null && !"".equals(tempEmailId)) {
			User userObj = service.fetchUserByEmailId(tempEmailId);
			if(userObj != null) {
				throw new Exception("User With "+tempEmailId+" is already Exist");
			}
		}
		User userObj = null;
		userObj = service.saveUser(user);
		return new ResponseEntity<User> (userObj,HttpStatus.OK);
	}
	
	@PostMapping(path = "/loginuser")
	@CrossOrigin(origins ="http://localhost:4200")
	public ResponseEntity<User> loginUser(@RequestBody User user) throws Exception {
		String tempEmailId = user.getEmailId();
		String tempPassword = user.getPassword();
		
		User userObj = null;
		
		if(tempEmailId != null && tempPassword != null) {
			userObj = service.fetchUserByEmailIdAndPssword(tempEmailId, tempPassword);
		}
		if(userObj == null) {
			throw new Exception("Bad Credentials");
		}
		return new ResponseEntity<User> (userObj,HttpStatus.OK);
	}
	@GetMapping(path = "/userid/{user}")
	@CrossOrigin(origins ="http://localhost:4200")
	public int getUser(@PathVariable ("user") String usermail) {
		User userObj = service.getUserId(usermail);
		int userid = userObj.getId();
		return userid;
	}
}
