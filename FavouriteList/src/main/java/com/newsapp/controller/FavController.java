package com.newsapp.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.newsapp.model.Favourite;
import com.newsapp.service.FavService;


@CrossOrigin("http://localhost:4200/")
@RestController
public class FavController {

	@Autowired
	private FavService service;

	/* Controller used for add to user favorite list */
	@PostMapping(path = "/addtolist")
	public ResponseEntity<Favourite> addtoList(@RequestBody Favourite fav) throws Exception {
		String tempTitle = fav.getTitle();
		int tempUserId = fav.getUserid();
		Favourite tempFav = service.fetchListByTitleAndUserid(tempTitle, tempUserId);
		if (tempFav != null) {
			throw new Exception("This Book is already in your List");
		}
		Favourite favObj = service.saveFav(fav);

		return new ResponseEntity<Favourite>(favObj,HttpStatus.CREATED);

	}
	
	
	/* Controller used for fetch the all favorite items of a user */
	@GetMapping(path = "/favlist/{userid}")
	public List<Favourite> getAllNews(@PathVariable("userid") int user) { 
		System.out.println(user);
		List<Favourite> allFavNews =  service.getAllFavNews(user);
		return allFavNews;
	}
	
	
	/* Controller used for delete the particular favorite item of a user */
	@DeleteMapping(path = "/favlist/{favid}")
	public ResponseEntity<Object> reomveFavBooks(@PathVariable ("favid") int favid) {
		Favourite favObj = service.fetchUserById(favid);
		service.deleteFavNewsById(favid);
		return new ResponseEntity<Object>(favObj,HttpStatus.OK);
	}
	
	/* Controller used for fetch all the favorite items of all user without repetition */
	@GetMapping(path = "/recommended")
	public List<Favourite> getRecNews() { 
		
		List <Favourite> newList = new ArrayList<Favourite>();
		List<Favourite> allFavNews =  service.getAllRecNews();
		System.out.println(allFavNews);
		for(Favourite i : allFavNews) {
			int flag =0;
			for(Favourite j : newList) {
				if(i.getUrl().equals(j.getUrl())) {
					System.out.println(i.getTitle());
					flag=1;
					break;
				}
			}
			if (flag==0) {
				newList.add(i);
			}
		}
		return newList;
	}
	
	
}
