package com.ust.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigInteger;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ust.model.User;
import com.ust.service.RegistrationService;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class RegistrationControllerTest {

	private MockMvc mockmvc;

	@Mock
	private RegistrationService userService;

	@InjectMocks
	private RegistrationController userController;

	@Autowired
	private User user;

	@BeforeEach
	void setUp() throws Exception {
		BigInteger mobile = new BigInteger("9714414922");
		user = new User(39, "demo@gmail.com", mobile, "demotest", "123456");
		mockmvc = MockMvcBuilders.standaloneSetup(userController).build();
	}

	@Test
	void givenUserThenCheckLoginOfUser() {
		String email = "admin@gmail.com";
		String pass = "admin";
		when(userService.fetchUserByEmailIdAndPssword(email, pass)).thenReturn(user);
		try {
			mockmvc.perform(post("/loginuser").contentType(MediaType.APPLICATION_JSON)
					.content(new ObjectMapper().writeValueAsString(user))).andExpect(status().isOk())
					.andDo(MockMvcResultHandlers.print());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	@Test
	void givenUserThenReturnRegisteredUser() {
		when(userService.saveUser(user)).thenReturn(user);

		try {
			mockmvc.perform(post("/registeruser").contentType(MediaType.APPLICATION_JSON)
					.content(new ObjectMapper().writeValueAsString(user))).andExpect(status().isOk())
					.andDo(MockMvcResultHandlers.print());

			verify(userService).saveUser(any());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
