package PageObjectModel;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SignupPageObjectModel {
	
	public WebDriver driver;

	public SignupPageObjectModel(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getEmail() {
		return driver.findElement(By.xpath("//input[contains(@name,'email')]"));
	}

	public WebElement getPhone() {
		return driver.findElement(By.xpath("//input[contains(@name,'mobile')]"));
	}
	public WebElement getPassword() {
		return driver.findElement(By.xpath("//input[contains(@type,'password')]"));
	}
	public WebElement getName() {
		return driver.findElement(By.xpath("//input[contains(@name,'name')]"));
	}
	public WebElement getMessage() {
		return driver.findElement(By.xpath("//small[contains(@name,'message')]"));
	}
	
	public WebElement getConfirmPass() {
		return driver.findElement(By.xpath("//input[contains(@name,'cpassword')]"));
	}
	
	public WebElement getImgupload() {
		return driver.findElement(By.xpath("//input[contains(@type,'file')]"));
	}
	
	public void uploadImg() {
		getImgupload().sendKeys("C:\\Users\\91894\\OneDrive\\Pictures\\1.jpg");
	}
	
	public void enterConfirm(String cpass) {
		getConfirmPass().sendKeys(cpass);
	}
	
	public void enterEmail(String mail) {
		getEmail().sendKeys(mail);
	}
	public void enterPhone(String phone) {
		getPhone().sendKeys(phone);
	}
	public void enterName(String name) {
		getName().sendKeys(name);
	}
	public void enterPassword(String pass) {
		getPassword().sendKeys(pass);
	}
	
	public WebElement getSubmitButton() {
		return driver.findElement(By.xpath("//button[contains(@type,'submit')]"));
	}

	public void clicksonSubmit() {
		getSubmitButton().click();
	}

}
