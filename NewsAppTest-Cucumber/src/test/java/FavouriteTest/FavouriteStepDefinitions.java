package FavouriteTest;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import PageObjectModel.FavouritePageObjectModel;
import PageObjectModel.LoginPageObjectModel;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

public class FavouriteStepDefinitions {
	
	WebDriver chromeDriver = null;
	FavouritePageObjectModel favouritepage = null;

	@Before // Hooks
	public void setUp() {
		WebDriverManager.chromedriver().setup();
		chromeDriver = new ChromeDriver();
	}
	@Given("Login page for App")
	public void login_page_for_app() {
		chromeDriver.get("http://localhost:4200/login");
		favouritepage = new FavouritePageObjectModel(chromeDriver);
		assertEquals(chromeDriver.getCurrentUrl(), "http://localhost:4200/login");
	}
	@When("user enters the email and password")
	public void user_enters_the_email_and_password(io.cucumber.datatable.DataTable dataTable) {
		List<String> aslist = dataTable.asList();
		favouritepage.enterEmail(aslist.get(0));
		favouritepage.enterPassword(aslist.get(1));
	}
	@When("user clicks login button")
	public void user_clicks_login_button() throws NoAlertPresentException,InterruptedException {
		favouritepage.clicksonSubmit();
		Thread.sleep(500);
		chromeDriver.switchTo().alert().accept();
	}
	@When("user clicks on Favourite navbar button")
	public void user_clicks_on_favourite_navbar_button() {
	    favouritepage.clicksonFavourite();
	}
	@Then("should navigate to favourite page")
	public void should_navigate_to_favourite_page() {
		assertEquals(chromeDriver.getCurrentUrl(), "http://localhost:4200/favourite");
	}
	@When("user clicks on remove button")
	public void user_clicks_on_remove_button() throws InterruptedException {
		Thread.sleep(500);
	    favouritepage.clicksonRemove();
	}
	@Then("should return alert message")
	public void should_return_alert_message() throws NoAlertPresentException,InterruptedException {
		Thread.sleep(500);
		assertEquals("News Removed successfully",chromeDriver.switchTo().alert().getText());
		chromeDriver.switchTo().alert().accept();
	}

	@When("user clicks on home button")
	public void user_clicks_on_home_button() throws InterruptedException {
		Thread.sleep(500);
	   favouritepage.clicksonHome();
	}
	@When("user clicks on Recommended button")
	public void user_clicks_on_recommended_button() throws InterruptedException {
		Thread.sleep(500);
		favouritepage.clicksonRecommend();
	}
	@Then("should navigate to Recommended page")
	public void should_navigate_to_recommended_page() throws InterruptedException {
		Thread.sleep(500);
		assertEquals(chromeDriver.getCurrentUrl(), "http://localhost:4200/recommended");
	}
	@Then("should navigate to home page")
	public void should_navigate_to_home_page() throws InterruptedException {
		Thread.sleep(500);
		assertEquals(chromeDriver.getCurrentUrl(), "http://localhost:4200/home");
	}
	
	@After
	public void closeBrowser() {
		chromeDriver.close();
	}
}








