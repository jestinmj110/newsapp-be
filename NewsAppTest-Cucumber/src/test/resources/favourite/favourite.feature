Feature: Test the Favourite functionality of App

Scenario: Favourite page with valid credentials
	Given Login page for App
	When user enters the email and password
		| admin@gmail.com	| admin |
	And user clicks login button
	And user clicks on Favourite navbar button
	Then should navigate to favourite page 
	
Scenario: Remove favourite with valid credentials
	Given Login page for App
	When user enters the email and password
		| admin@gmail.com	| admin |
	And user clicks login button
	And user clicks on Favourite navbar button
	And user clicks on remove button
	Then should return alert message
	
 Scenario: Home from favourite page
	Given Login page for App
	When user enters the email and password
		| admin@gmail.com	| admin |
	And user clicks login button
	And user clicks on Favourite navbar button
	And user clicks on home button
	Then should navigate to home page
	
Scenario: Recommended from favourite page
	Given Login page for App
	When user enters the email and password
		| admin@gmail.com	| admin |
	And user clicks login button
	And user clicks on Favourite navbar button
	And user clicks on Recommended button
	Then should navigate to Recommended page